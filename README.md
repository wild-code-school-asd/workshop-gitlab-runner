# workshop-gitlab-runner

## To-do

- [ ] Step 1: create a new **LXC container** (CT) on **NODE01** (srv proxmox wild code school)
  - Template: Ubuntu 22.04
  - Storage: "local-nvme-datas" with 4gb
  - CPU: 1 Cores
  - Memory: 2gb
  - Network:
    - interface: vmbr2
    - IPv4/CIDR: 192.168.1.`<lxc_id>`/24
    - Gateway (IPv4): 192.168.1.254
- [ ] Step 2: follow the step [describes here](https://www.linuxtechi.com/how-to-install-gitlab-runner-on-ubuntu/) to install a Gitlab Runner with apt and specific Gitlab repository
- [ ] Step 3: Add this Runner on a Gitlab CE instance
- [ ] Step 4: create an ansible's role that will install a Gitlab Runner on a remote managed node
- [ ] Step 5: create the playbook that allows the play of these ansible's roles
- [ ] Step 6: Fill in the "Getting Started" part with instructions on the manual installation of a Gitlab Runner
- [ ] Step 7: Fill in the "Getting Started" part with instructions on the installation's of Gitlab CE with your Ansible's role
- [ ] Step 8: push your project to gitlab or github and make the repository accessible to your trainer

## Getting started

@todo
